import { BaseCarouselItem } from '../../molecules/Carousel/types';
import { PeopleTileProps } from '../../molecules/PeopleTile/types';

export type PeopleCarouselItem = PeopleTileProps & BaseCarouselItem;

export interface PeopleCarouselProps {
  items: PeopleCarouselItem[];
  title?: string;
}
