import { ArticleTileProps } from '../../molecules/ArticleTile/types';

export interface ArticlesListingItem extends ArticleTileProps {
  key: string;
}

export interface ArticlesListingProps {
  items: ArticlesListingItem[];
  nextPage: () => void;
  totalItems: number;
  isLastPage?: boolean;
}
