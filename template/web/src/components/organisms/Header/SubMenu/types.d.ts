import { HeaderMenuItem } from '../types';

export interface HeaderSubMenuProps {
  items: HeaderMenuItem[];
  isScrollable?: boolean;
}
