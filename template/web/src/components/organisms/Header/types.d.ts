export interface HeaderMenuItem {
  label: string;
  to: string;
  mobileHidden?: boolean;
}
export type IIsPreviewPageSlugInNavItems = (
  a: string,
  b: string,
  c: HeaderMenuItem[],
) => boolean;

export type Submenu = {
  slug: ISlug;
  title: string;
};

export type HeaderDataQuery = {
  menuItems: {
    elements: {
      _key: string;
      slug?: {
        slug: ISlug;
      };
      url?: string;
      linkType?: 'external' | 'internal';
      title: string;
      submenu?: Submenu[];
    }[];
  };
};
