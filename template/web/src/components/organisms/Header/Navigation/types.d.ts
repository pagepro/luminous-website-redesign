import { HeaderMenuItem } from '../types';

export interface HeaderNavigationItem extends HeaderMenuItem {
  subMenu?: HeaderMenuItem[];
  key: string;
  activeUrls?: string[];
  linkType?: 'external' | 'internal';
}

export type ContentComponentNavigationItem = Omit<HeaderNavigationItem, 'key'>;

export interface HeaderNavigationItemProps {
  item: ContentComponentNavigationItem;
}

export interface HeaderNavigationProps {
  items: HeaderNavigationItem[];
}
