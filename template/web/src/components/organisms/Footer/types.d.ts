import { IMenuItem } from '../../../apiTypes';

export interface FooterInfoLinkItem {
  label: string;
  href?: string;
  to?: string;
  key: string;
}

export interface FooterInfoItem {
  title: string;
  items: FooterInfoLinkItem[];
  key: string;
}

export interface FooterInfoPropsItem {
  item: FooterInfoItem;
}
export interface FooterInfoProps {
  items: FooterInfoItem[];
}

export interface FooterCopyLink {
  label: string;
  to: string;
  key: string;
}

export interface FooterCopyProps {
  links: FooterCopyLink[];
}

export type FooterProps = FooterCopyProps & FooterInfoPropsItem;

export type FooterDataQuery = {
  footerItems: {
    edges: {
      node: {
        id: string;
        title: string;
        items: IMenuItem[];
      };
    }[];
  };
};
