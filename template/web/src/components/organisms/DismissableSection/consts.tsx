/** @jsx jsx */
import { jsx } from 'theme-ui';
import React from 'react';
import { Link } from 'gatsby';
import { DismissableSectionItem } from './types';

export const dismissableSectionDemoItems: DismissableSectionItem[] = [
  {
    key: '1',
    category: 'Next event',
    content: (
      <React.Fragment>
        <h3>Our #LifeAfter&#8203;Lockdown webinar series</h3>
        <p>
          Looking beyond the current crisis management to the future, bringing
          together industry specialists, a range of clients and Luminous
          strategists to share thinking and ideas about the key areas of
          communication and engagement that businesses will most need to focus
          on in the new normal.
        </p>
        <Link to="#register-link" sx={{ variant: 'links.underlined' }}>
          Register
        </Link>
      </React.Fragment>
    ),
    imageSrc: '/assets/images/carousel-1.jpg',
    imageAlt: 'Image alt',
    whiteColorText: false,
  },
  {
    key: '2',
    category: 'Article',
    content: (
      <React.Fragment>
        <h3>The most important Annual Report you will ever produce</h3>
        <p>
          An expert panel of speakers joined Stephen Butler and Harriet Rumball
          of the Luminous Investor Engagement strategy team. Read a selection of
          key take-aways from each of the panellists’ presentations:
        </p>
        <Link to="#register-link" sx={{ variant: 'links.underlinedLight' }}>
          Take a look
        </Link>
      </React.Fragment>
    ),
    imageSrc: '/assets/images/carousel-2.jpg',
    imageAlt: 'Image alt',
    whiteColorText: true,
  },
  {
    key: '3',
    category: 'Next event',
    content: (
      <React.Fragment>
        <h3>Our #LifeAfter&#8203;Lockdown webinar series 3</h3>
        <p>
          Looking beyond the current crisis management to the future, bringing
          together industry specialists, a range of clients and Luminous
          strategists to share thinking and ideas about the key areas of
          communication and engagement that businesses will most need to focus
          on in the new normal. engagement that businesses will most need to
          focus on in the new normal.
        </p>
      </React.Fragment>
    ),
    imageSrc: '/assets/images/carousel-1.jpg',
    imageAlt: 'Image alt',
    whiteColorText: false,
  },
];
