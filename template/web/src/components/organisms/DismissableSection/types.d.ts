import { BaseCarouselItem } from '../../molecules/Carousel/types';
import { DismissableSectionContentProps } from './content/types';

export type DismissableSectionItem = BaseCarouselItem &
  DismissableSectionContentProps;

export interface DismissableSectionNavigationProps {
  dots?: React.ReactNode;
  onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  isLight?: boolean;
}

export interface DismissableSectionProps {
  items: DismissableSectionItem[];
}
