export interface DismissableSectionContentProps {
  content: JSX.Element;
  imageSrc: string;
  imageAlt: string;
  category: string;
  whiteColorText: boolean;
}
