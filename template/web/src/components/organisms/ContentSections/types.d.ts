import { CaseStudyPageProps } from '../../pages/caseStudy/types';
import { IContentSection } from '../../../apiTypes';

export type IContentSectionsProps = {
  items: CaseStudyPageProps['data']['caseStudy']['sections'];
};
export type ISection = IContentSection & {
  data: IContentSection[];
};
