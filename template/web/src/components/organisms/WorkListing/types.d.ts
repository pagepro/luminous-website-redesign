import { ThumbnailTileProps } from '../../molecules/ThumbnailTile/types';

export interface WorkListingItem extends ThumbnailTileProps {
  key: string;
  small?: boolean;
}

export interface WorkListingProps {
  items: WorkListingItem[];
}
