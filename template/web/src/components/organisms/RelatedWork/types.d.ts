import { ThumbnailTileProps } from '../../molecules/ThumbnailTile/types';

export interface RelatedWorkItem extends ThumbnailTileProps {
  key: string;
}

export interface RelatedWorkProps {
  items: RelatedWorkItem[];
}
