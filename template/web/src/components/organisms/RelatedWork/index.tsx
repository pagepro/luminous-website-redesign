import React from 'react';
import { Box, Flex, Text } from 'theme-ui';
import ThumbnailTile from '../../molecules/ThumbnailTile';
import { RelatedWorkProps } from './types';

const RelatedWork: React.FC<RelatedWorkProps> = ({ items }) => (
  <Box>
    <Text as="h2" variant="h4roman" mb={['1rem', '', '2rem']}>
      Related work
    </Text>
    <Flex as="ul" variant="relatedWork.list">
      {items.map(({ key, ...item }) => (
        <Flex as="li" {...{ key }} variant="relatedWork.listItem">
          <ThumbnailTile small notAnimated {...item} />
        </Flex>
      ))}
    </Flex>
  </Box>
);

export default RelatedWork;
