import { InfoBoxProps } from '../../molecules/InfoBox/types';

export interface InfoBoxListItem extends InfoBoxProps {
  key: string;
}

export interface InfoBoxListProps {
  items: InfoBoxListItem[];
}
