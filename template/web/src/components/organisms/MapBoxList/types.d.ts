import { MapBoxProps } from '../../molecules/MapBox/types';

export interface MapBoxListItem extends MapBoxProps {
  key: string;
}

export interface MapBoxListProps {
  items: MapBoxListItem[];
}
