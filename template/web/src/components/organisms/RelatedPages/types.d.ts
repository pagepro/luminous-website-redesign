import { RelatedPageTileProps } from '../../molecules/RelatedPageTile/types';

export interface RelatedPageItem extends RelatedPageTileProps {
  key: string;
}

export interface RelatedPagesProps {
  items: RelatedPageItem[];
}
