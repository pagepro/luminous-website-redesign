import { ArticleTileProps } from '../../molecules/ArticleTile/types';
import { BaseCarouselItem } from '../../molecules/Carousel/types';

export type ArticlesCarouselItem = BaseCarouselItem & ArticleTileProps;

export type ArticlesCarouselContentProps = Omit<ArticlesCarouselItem, 'key'>;

export interface ArticlesCarouselProps {
  items: ArticlesCarouselItem[];
}
