export default {
  wrapper: {
    width: '100%',
    '.slick-dots': {
      marginTop: ['3.125rem', '5.625rem', '3.125rem'],
    },
  },
};
