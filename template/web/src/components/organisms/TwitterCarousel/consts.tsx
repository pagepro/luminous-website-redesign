import React from 'react';
import { TwitterCarouselItem } from './types';

export const twitterCarouselItems: TwitterCarouselItem[] = [
  {
    author: '@Luminous-design',
    content: (
      <>
        In this week&apos;s #LuminousSpotlight, Senior Sustainability
        Consultant, James Whittingham explores how the mainstream investment
        community is shifting towards #ESG investments{' '}
        <a href="http://bit.ly/2KoW8Z5" target="_blank" rel="noreferrer">
          http://bit.ly/2KoW8Z5
        </a>
      </>
    ),
    key: '1',
  },
  {
    author: '@Luminous-design 2',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
    key: '2',
  },
  {
    author: '@Luminous-design 3',
    content:
      'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui',
    key: '3',
  },
];
