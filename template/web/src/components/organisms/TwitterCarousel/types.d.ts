import { BaseCarouselItem } from '../../molecules/Carousel/types';

export interface TwitterCarouselItem extends BaseCarouselItem {
  author: string;
  content: string | JSX.Element;
  link?: string;
}

export type TwitterCarouselContentProps = Omit<TwitterCarouselItem, 'key'>;

export interface TwitterCarouselProps {
  items: TwitterCarouselItem[];
}
