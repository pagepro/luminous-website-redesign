import { ArticlesCarouselItem } from '../ArticlesCarousel/types';

export interface ArticlesSectionProps {
  title?: string;
  items: ArticlesCarouselItem[];
  seeAllHref?: string;
  seeAllText?: string;
}
