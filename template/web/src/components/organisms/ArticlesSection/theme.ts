import text from '../../../gatsby-plugin-theme-ui/text';

export default {
  title: {
    marginBottom: ['1rem', '', '2rem'],
    ...text.h4black,
  },
};
