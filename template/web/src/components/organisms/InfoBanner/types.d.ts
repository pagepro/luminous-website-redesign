import { NewsletterModalProps } from '../../molecules/Modal/templates/NewsletterModal/types';

export interface InfoBannerItem {
  title: string;
  content: JSX.Element;
  key: string;
}

export interface InfoBannerProps {
  items: InfoBannerItem[];
  modal?: Omit<NewsletterModalProps, 'onClose'>;
}
