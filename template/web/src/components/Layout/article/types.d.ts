import { ImageProps } from '../../atoms/Image/types';

export interface ArticleLayoutAuthor {
  image: ImageProps;
  name: string;
  email?: string;
}

export interface ArticleLayoutProps {
  date: string;
  title: string;
  subtitle?: string;
  author: ArticleLayoutAuthor;
  image?: ImageProps;
  content: JSX.Element;
}
