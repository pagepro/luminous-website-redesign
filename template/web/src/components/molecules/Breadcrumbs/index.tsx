/** @jsx jsx */
import { jsx, Box, Container, Flex, Text } from 'theme-ui';
import { Link } from 'gatsby';
import { BreadcrumbsProps } from './types';

const Breadcrumbs: React.FC<BreadcrumbsProps> = ({ items }) => (
  <Box variant="breadcrumbs.wrapper">
    <Container>
      <Flex as="ul" variant="breadcrumbs.list">
        {items.map(({ label, to, key }) => (
          <Flex as="li" {...{ key }} variant="breadcrumbs.listItem">
            {to ? (
              <Link {...{ to }} sx={{ variant: 'breadcrumbs.linkText' }}>
                {label}
              </Link>
            ) : (
              <Text as="span" variant="breadcrumbs.text">
                {label}
              </Text>
            )}
          </Flex>
        ))}
      </Flex>
    </Container>
  </Box>
);

export default Breadcrumbs;
