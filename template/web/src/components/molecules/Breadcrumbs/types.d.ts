export interface BreadcrumbsItem {
  label: string;
  to?: string;
  key: string;
}

export interface BreadcrumbsProps {
  items: BreadcrumbsItem[];
}
