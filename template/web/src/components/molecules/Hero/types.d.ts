export interface HeroProps {
  heading1: string;
  heading2: JSX.Element;
  isHeading1Wide?: boolean;
}
