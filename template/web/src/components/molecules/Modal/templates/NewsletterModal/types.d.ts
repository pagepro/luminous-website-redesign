import { ModalBaseCommonProps } from '../../types';

export interface NewsletterModalProps
  extends Omit<ModalBaseCommonProps, 'withCloseIcon'> {
  title: string;
  portalId: string;
  formId: string;
  onSubmit: () => void;
  onReady?: () => void;
}
