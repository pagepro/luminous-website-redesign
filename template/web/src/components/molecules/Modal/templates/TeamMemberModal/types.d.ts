import { ImageProps } from '../../../../atoms/Image/types';
import { ModalBaseCommonProps } from '../../types';

export interface TeamMemberModalProps
  extends Omit<ModalBaseCommonProps, 'withCloseIcon'> {
  image: ImageProps;
  name: string;
  profession: string | string[];
  description: JSX.Element;
  email: string;
}
