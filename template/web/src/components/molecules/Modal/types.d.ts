import { MutableRefObject } from 'react';

export interface ModalWrapperProps {
  isHorizontal?: boolean;
  isAlignedRight?: boolean;
  isDark?: boolean;
}

export interface ModalBaseCommonProps {
  onClose: (event?: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  withCloseIcon?: boolean;
}

export type ModalBaseProps = ModalBaseCommonProps & ModalWrapperProps;

export interface UseModalContextValueReturnType {
  modalContextValue: HTMLDivElement | undefined;
  modalContainerRef: MutableRefObject<HTMLDivElement | null>;
}
