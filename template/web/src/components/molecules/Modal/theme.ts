import teamMember from './templates/TeamMemberModal/theme';
import newsletter from './templates/NewsletterModal/theme';

export default {
  teamMember,
  newsletter,
};
