import { ImageProps } from '../../atoms/Image/types';
import { VideoProps } from '../Video/types';

export interface InfoGridItem {
  title: string;
  content: string | string[];
  modal?: TeamMemberModalProps;
  key: string;
}

export interface InfoGridItemProps extends Omit<InfoGridItem, 'key'> {
  wide?: boolean;
}

export interface InfoGridStyledWrapperProps {
  withVideo?: boolean;
  reversed?: boolean;
}

export type InfoGridMediaProps = VideoProps & ImageProps;

export interface InfoGridListProps {
  columnsCount: number[];
  columnGap: string[];
  items: InfoGridItem[];
  wide?: boolean;
}

export interface InfoGridProps extends Omit<InfoGridListProps, 'wide'> {
  title?: string;
  isHorizontal?: boolean;
  media?: InfoGridMediaProps;
  reversed?: boolean;
}
