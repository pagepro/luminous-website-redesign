export interface InfoBoxProps {
  title: string;
  description: string;
  to: string;
  ctaLabel: string;
}
