import { ImageProps } from '../../atoms/Image/types';

export interface PeopleTileProps {
  name: string;
  profession: string;
  title: string;
  quote: string;
  image: ImageProps;
}
