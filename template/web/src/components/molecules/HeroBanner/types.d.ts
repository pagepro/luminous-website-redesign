import { VideoProps } from '../Video/types';
import { ImageProps } from '../../atoms/Image/types';

export interface HeroBannerProps {
  image?: ImageProps;
  video?: VideoProps;
}
