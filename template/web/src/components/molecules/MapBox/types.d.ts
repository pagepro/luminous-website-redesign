import { ImageProps } from '../../atoms/Image/types';

export interface MapBoxProps {
  title: string;
  id?: string;
  address: string[];
  phoneNumber?: string;
  zoomLink?: string;
  href: string;
  map: ImageProps;
  pinPercentagePosition: { x: string[]; y: string[] };
}
