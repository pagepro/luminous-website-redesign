import { ImageProps } from '../../atoms/Image/types';
import { VideoProps } from '../Video/types';

export interface ImagesGridItem {
  small?: boolean;
  key: string;
  image?: ImageProps;
  video?: VideoProps;
}

export interface ImagesGridProps {
  items: ImagesGridItem[];
}
