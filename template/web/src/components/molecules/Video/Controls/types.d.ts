import { Range } from 'react-input-range';

export interface VideoControlsProps {
  handlePlayPause: (value?: boolean) => void;
  handleMute: () => void;
  isPlaying?: boolean;
  isMuted?: boolean;
  videoProgress: number;
  handleVideoProgress: (value: number | Range) => void;
}
