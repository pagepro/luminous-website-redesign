import { ReactPlayerProps } from 'react-player';
import { ImageProps } from '../../atoms/Image/types';

export interface VideoProps extends Pick<ReactPlayerProps, 'url'> {
  image?: ImageProps;
  small?: boolean;
  autoPlay?: boolean;
  noControls?: boolean;
}
