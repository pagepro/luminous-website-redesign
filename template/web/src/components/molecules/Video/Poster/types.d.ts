import { ImageProps } from '../../../atoms/Image/types';

export interface VideoPosterProps {
  onClick: () => void;
  image: ImageProps;
}
