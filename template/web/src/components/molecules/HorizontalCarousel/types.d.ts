import { BaseCarouselItem } from '../Carousel/types';

export interface HorizontalCarouselProps<T extends BaseCarouselItem> {
  items: T[];
  component: React.FC<Omit<T, 'key'>>;
  sectionTitle?: string;
}
