export enum CmsContentType {
  default = 'wrapper',
  simple = 'wrapperSimple',
  article = 'wrapperArticle',
  customLink = 'wrapperCustomLink',
}
