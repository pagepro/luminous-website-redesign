import { CmsContentType } from './enums';

export interface CmsContentSectionProps {
  title?: string;
  items: string[];
}

export interface CmsContentProps {
  type?: CmsContentType;
}
