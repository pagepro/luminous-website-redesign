import { Settings } from 'react-slick';

export interface BaseCarouselItem {
  key: string;
}

export interface CarouselProps<T extends BaseCarouselItem> extends Settings {
  items: T[];
  component: React.FC<Omit<T, 'key'>>;
}
