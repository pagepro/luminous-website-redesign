import { ImageProps } from '../../atoms/Image/types';

export interface ArticleTileProps {
  tag: string;
  title: string;
  description: string;
  image: ImageProps;
  to: string;
  isFeatured?: boolean;
}
