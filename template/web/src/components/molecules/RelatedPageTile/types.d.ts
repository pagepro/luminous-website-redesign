import { ImageProps } from '../../atoms/Image/types';

export interface RelatedPageTileProps {
  title: JSX.Element;
  ctaText: string;
  to: string;
  image: ImageProps;
  isWorkRelated?: boolean;
}
