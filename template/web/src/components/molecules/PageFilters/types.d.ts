export interface PageFilterItem {
  label: string;
  to: string;
  key: string;
}

export interface PageFiltersProps {
  items: PageFilterItem[];
}
