import { ImageProps } from '../../atoms/Image/types';
import { VideoProps } from '../Video/types';

export interface ThumbnailTileProps {
  to: string;
  image: ImageProps;
  video?: VideoProps;
  category: string;
  title: string;
  dark?: boolean;
  small?: boolean;
  notAnimated?: boolean;
}
