/** @jsx jsx */
import React from 'react';
import { jsx, Text } from 'theme-ui';

import { InfoBannerItem } from '../../organisms/InfoBanner/types';

export const infoBannerItems: InfoBannerItem[] = [
  {
    title: 'Keep in touch',
    key: '1',
    content: (
      // eslint-disable-next-line react/jsx-fragments
      <React.Fragment>
        <Text as="p" variant="h2" mb={['1rem', '1rem', '2.5rem']}>
          Sign up to our newsletter to receive regular updates from Luminous.
        </Text>
      </React.Fragment>
    ),
  },
];