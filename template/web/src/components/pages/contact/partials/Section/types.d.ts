export interface ContactPageSectionProps {
  title: JSX.Element | string;
}
