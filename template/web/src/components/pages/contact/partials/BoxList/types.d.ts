export interface ContactPageBoxItemContentProps {
  title?: string;
  name: string;
  profession: string;
  phoneNumber?: string;
  zoomLink?: string;
  email?: string;
}

export interface ContactPageBoxContentItem
  extends ContactPageBoxItemContentProps {
  key: string;
}

export interface ContactPageBoxItem {
  title: string;
  items: ContactPageBoxContentItem[];
  key: string;
}

export type ContactPageBoxItemProps = Omit<ContactPageBoxItem, 'key'>;

export interface ContactPageBoxListProps {
  items: ContactPageBoxItem[];
}
