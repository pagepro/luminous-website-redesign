import section from './partials/Section/theme';
import box from './partials/BoxList/theme';

export default {
  section,
  box,
};
