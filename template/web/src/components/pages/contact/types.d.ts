import { PageProps } from 'gatsby';

type ImageNames = 'picMap01' | 'picMap02';

type PageData = GQLImages<ImageNames>;

export type ContactPageProps = PageProps<PageData>;
