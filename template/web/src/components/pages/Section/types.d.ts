import { BoxProps } from 'theme-ui';

export interface PageSectionProps extends BoxProps {
  hasSmallSpacing?: boolean;
}
