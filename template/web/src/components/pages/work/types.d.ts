// import { TransitionStatuses } from 'gatsby-plugin-transition-link';
import { ICasyStudyThumbnail, ICategory } from '../../../apiTypes';

export type IWorksPageData = {
  categories: ICategoriesNodes;
  works: IWorksList;
};

interface ISingleCaseStudy {
  _type: 'singleCaseStudy';
  caseStudy: ICasyStudyThumbnail[];
}
interface ITwoCaseStudies extends ISingleCaseStudy {
  _type: 'twoCaseStudies';
  caseStudySecond: ICasyStudyThumbnail[];
}

export type IWorksList = {
  caseStudies: (ISingleCaseStudy | ITwoCaseStudies)[];
  seoTitle: string;
  seoDescription: string;
  seoImage?: ISeoImage;
};

export type WorkPageProps = {
  data: IWorksPageData;
  // transitionStatus?: TransitionStatuses;
};

export type ICategoriesNodes = {
  nodes: ICategory[];
};
