// import { TransitionStatuses } from 'gatsby-plugin-transition-link';
import { PageFilterItem } from '../../../molecules/PageFilters/types';
import { WorkListingItem } from '../../../organisms/WorkListing/types';

export interface WorkpageHomeProps {
  items: WorkListingItem[];
  filters: PageFilterItem[];
  // transitionStatus?: TransitionStatuses;
}
