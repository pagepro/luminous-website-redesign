import routes from '../../../routes';
import { HeaderMenuItem } from '../../organisms/Header/types';

const { ABOUT_BRANDING, ABOUT_REPORTING, ABOUT_SUSTAINABILITY } = routes;

export const aboutSubMenu: HeaderMenuItem[] = [
  {
    label: 'Brand & Comms',
    to: ABOUT_BRANDING,
  },
  {
    label: 'Investor engagement',
    to: ABOUT_REPORTING,
  },
  {
    label: 'Sustainable business',
    to: ABOUT_SUSTAINABILITY,
  },
];
