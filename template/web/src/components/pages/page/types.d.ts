/* eslint-disable camelcase */
import {
  ICarouselItem,
  ICasyStudyThumbnail,
  IMenuItem,
  ITeamMember,
  ICarouselQuote,
  IInfoBanner,
  IInfoBannerPopup,
} from '../../../apiTypes';

export type IPageData = {
  data: {
    page: IPage;
  };
  pageContext: {
    lastInsights: IInsightThumbnail[]
  }
};
export type ISectionsProps = {
  sections: IPageSections;
};

export type IPage = {
  title: string;
  hasSubmenu: boolean;
  showTwitterSection: boolean;
  seoTitle: string;
  seoDescription: string;
  seoImage?: ISeoImage;
  carouselItems: ICarouselItem[];
  carouselQuotes: ICarouselQuote[];
  infoBanner: (IInfoBanner | IInfoBannerPopup)[];
  sections: IPageSections;
  slides: ISlide[];
  showSectionSlider: boolean;
};

type IPageSections = (
  | IHeadingContentHighlight
  | ISingleImage
  | IVideoUrl
  | IVideoMP4
  | IContentText
  | IContentButton
  | ICaseStudiesSection
  | IInsightsSection
  | IStickyListSection
  | IListsWithSublists
  | ITeamMembersSection
  | IRelatedPagesSection
  | IJobsSection
  | IContactCards
  | IMapListSection
)[];

export type IHeadingContentHighlight = {
  _type: 'headingContentHighlight';
  _key: string;
  heading: string;
  _rawBody: IContent[];
};
export type ISingleImage = {
  _type: 'singleImage';
  _key: string;
  mainImage: IImage;
};
export type IVideoUrl = {
  _type: 'videoUrl';
  _key: string;
  isAutoplay: boolean;
  videoUrl: string;
  placeholderImage: IImage;
};
export type IVideoMP4 = {
  _type: 'video';
  _key: string;
  isAutoplay: boolean;
  video: IVideo;
  placeholderImage: IImage;
};
export type IContentButton = {
  _type: 'contentButton';
  _key: string;
  _rawBody: IContent[];
  button: IMenuItem[];
};
export type IContentText = {
  _type: 'contentText';
  _key: string;
  _rawBody: IContent[];
  text: string;
};
export type ICaseStudiesSection = {
  _type: 'caseStudiesSection';
  _key: string;
  button: IMenuItem[];
  title: string;
  caseStudies: ICasyStudyThumbnail[];
};
export type IJobsSection = {
  _type: 'jobsSection';
  _key: string;
  jobs: JobShort[];
};
type JobShort = {
  id: string;
  name: string;
  description: string;
  slug: ISlug;
};
export type IStickyListSection = {
  _type: 'stickyList';
  _key: string;
  title: string;
  isReverseColumn: boolean;
  mediaItem?: (IVideoUrl | IVideoMP4 | IImage)[];
  list: headingText[];
};
export type headingText = {
  _key: string;
  title: string;
  text: string;
};
export type IListsWithSublists = {
  _type: 'listsWithSublists';
  _key: string;
  title: string;
  listItems: headingListitems[];
};
export type headingListitems = {
  _key: string;
  heading: string;
  list: string[];
};

export type ITeamMembersSection = {
  _type: 'teamMembersSection';
  _key: string;
  title: string;
  teamMembers: ITeamMember[];
};
export type IInsightsSection = {
  _type: 'insightsSection';
  _key: string;
  title: string;
  shouldIgnoreSelection?: boolean
  insights: IInsightThumbnail[];
  button: IMenuItem[];
};

type IInsightThumbnail = {
  author: string;
  date: string;
  description: string;
  id: string;
  imageUrl: string;
  title: string;
  slug: ISlug;
};
export type IRelatedPagesSection = {
  _type: 'relatedPages';
  _key: string;
  pagesList: IRelatedPage[];
};
export type IContactCards = {
  _type: 'contactCards';
  _key: string;
  contactCards: IContactCard[];
};
export type IContactCard = {
  _key: string;
  title: string;
  personInfo: IPersonInfo[];
};
export type IPersonInfo = {
  _key: string;
  title: string;
  name: string;
  profession: string;
  email?: string;
  phoneNumber?: string;
};
export type IRelatedPage = {
  _key: string;
  _rawBody: IContent[];
  url: {
    slug: ISlug;
  };
  image: IImage;
  buttonText: string;
  isWorkRelated?: boolean;
};
export type IMapListSection = {
  _type: 'mapList';
  _key: string;
  title: string;
  mapList: IMapList[];
};
export type IMapList = {
  _key: string;
  title?: string;
  address?: string;
  phoneNumber?: string;
  zoom?: string;
  mapUrl?: string;
  image?: IImage;
  pinPositionX?: IPositions;
  pinPositionY?: IPositions;
};

type IPositions = {
  mobile: number;
  tablet: number;
  desktop: number;
};

type IOutputCarouselQuotes = {
  items: PeopleCarouselItem[];
  title: string;
};

export type IGetCarouselQuotes = (
  item: ICarouselQuote,
) => IOutputCarouselQuotes;

export type TwitterQuery = {
  tweets: {
    nodes: {
      id_str: string;
      user: {
        screen_name: string;
      };
      full_text: string;
    }[];
  };
};

type ISlide = {
  _key: string;
  title: string;
  setWhiteColorText: boolean;
  description: string;
  buttonLink: string;
  buttonText: string;
  tag: string;
  image: IImage;
};
