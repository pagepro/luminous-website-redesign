import React from 'react';
import { Link, Text } from 'theme-ui';
import { InfoBannerItem } from '../../organisms/InfoBanner/types';

export const tagsItems: string[] = ['Annual reporting', 'Video', 'Digital'];

export const infoBannerItems: InfoBannerItem[] = [
  {
    title: 'Michal Kendereski, Digital Director',
    key: '1',
    content: (
      <Text as="p" variant="h4roman">
        The best digital solutions are an effective marriage of creative flair
        and innovation. Under the elegant interface lay smart use of
        technologies, that empowered Maesa to expand the site, without the need
        for ongoing development investment.
      </Text>
    ),
  },
  {
    title: 'Have a similar challenge? Get in touch.',
    key: '2',
    content: (
      <Link
        href="mailto:justin.boucher@luminous.co.uk"
        target="_blank"
        rel="noreferrer"
        variant="links.underlinedLight"
      >
        justin.boucher@luminous.co.uk
      </Link>
    ),
  },
];
