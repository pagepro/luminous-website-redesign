import { PageProps } from 'gatsby';
import { ICasyStudy } from '../../../apiTypes';

type ImageNames =
  | 'picHero'
  | 'picVideo01'
  | 'picVideo02'
  | 'picCaseStudy01'
  | 'picCaseStudy02'
  | 'picCaseStudy03'
  | 'picCaseStudy04'
  | 'picCaseStudy05'
  | 'picCaseStudy06'
  | 'picCaseStudy07'
  | 'picRelatedWork01'
  | 'picRelatedWork02';

// interface PageData extends GQLImages<ImageNames> { }
type PageData = GQLImages<ImageNames> & {
  caseStudy: ICasyStudy;
};

export type CaseStudyPageProps = PageProps<PageData>;
export type CaseStudyBodyProps = {
  data: {
    caseStudy: ICasyStudy;
  };
};
