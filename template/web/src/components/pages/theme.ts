import page from './page/theme';
import section from './Section/theme';
import contact from './contact/theme';

export default {
  page,
  section,
  contact,
};
