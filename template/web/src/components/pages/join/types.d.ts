import { PageProps } from 'gatsby';

type ImageNames =
  | 'picVideo01'
  | 'picVideo02'
  | 'picVideo03'
  | 'picPeople01'
  | 'picPeople02'
  | 'picJoin01'
  | 'picJoin02';

// interface PageData extends GQLImages<ImageNames> { }
type PageData = GQLImages<ImageNames>;
type JoinOpportunitiesData = {
  jobs: {
    nodes: JobShort[];
  };
};
type JobData = {
  job: IJob;
};

export type JobPageProps = PageProps<JobData>;

type IJob = {
  date: string;
  name: string;
  email: string;
  _rawSummary: IContent[];
  _rawOverview: IContent[];
};
type JobShort = {
  id: string;
  name: string;
  description: string;
  slug: ISlug;
};
