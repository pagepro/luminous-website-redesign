/* eslint-disable camelcase */
import { PageProps } from 'gatsby';

type ImageNames =
  | 'picInsight01'
  | 'picInsight02'
  | 'picInsight03'
  | 'picInsight04'
  | 'picPeople01';

// interface PageData extends GQLImages<ImageNames> { }
type PageData = GQLImages<ImageNames>;

export type InsightPageProps = PageProps<PageData>;

export type InsightTemplateProps = {
  pageContext: {
    post: IInsightsPost;
    relatedPosts: IInsightsPost[];
  };
};
export type InsightsTemplateProps = {
  pageContext: {
    posts: IInsightsPost[];
    tags: IInsightTag[];
  };
};

export type IInsightTag = {
  id: number;
  name: string;
  slug: string;
};

export type IInsightsPost = {
  id: number;
  slug: string;
  title: string;
  post_list_content: string;
  post_body: string;
  post_summary: string;
  blog_post_author: IInsightPostAuthor;
  publish_date: number;
  page_title: string;
  meta_description: string;
  featured_image: string;
  featured_image_alt_text: string;
  post_list_summary_featured_image: string;
  widgets: {
    post_banner_image: {
      body: IInsightsPostThumbnailImage;
    };
  };
};
export type IInsightPostAuthor = {
  name: string;
  avatar: string;
  email: string;
};

export type IInsightsPostThumbnailImage = {
  src: string;
};
