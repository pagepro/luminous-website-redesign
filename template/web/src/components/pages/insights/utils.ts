import Moment from 'moment';
import unescape from 'lodash/unescape';
import routes from '../../../routes';
import { generatePath } from '../../../utils/generatePath';
import { ImageProps } from '../../atoms/Image/types';
import { ArticleLayoutAuthor } from '../../Layout/article/types';
import { PageFilterItem } from '../../molecules/PageFilters/types';
import { ArticlesListingItem } from '../../organisms/ArticlesListing/types';
import { getFirstWord } from '../page/utils';
import { IInsightPostAuthor, IInsightsPost, IInsightTag } from './types';

export const getPostImage = (post: IInsightsPost): ImageProps => ({
  src: `${
    post.post_list_summary_featured_image
      ? post.post_list_summary_featured_image
      : post.featured_image
  }`,
  alt: post.featured_image_alt_text
    ? post.featured_image_alt_text
    : post.page_title,
});

export const stripTags = (str: string): string =>
  unescape(str.replace(/<\/?[^>]+(>|$)/g, '').replace(/&nbsp;/g, ' '));

export const getDate = (timestamp: number, format = 'DD.MM.YY'): string =>
  Moment(timestamp).format(format);

export const getInsightsItems = (
  posts: IInsightsPost[] = [],
  hasFeatured = true,
): ArticlesListingItem[] =>
  posts.map((post, index) => ({
    tag: `${post.blog_post_author.name}  /  ${getDate(post.publish_date)}`,
    title: post.title,
    description: stripTags(post.post_list_content),
    isFeatured: hasFeatured && index <= 1,
    to: generatePath(routes.INSIGHT_ARTICLE, {
      slug: post.slug,
    }),
    image: getPostImage(post),
    key: String(post.id),
  }));

export const getPaginatedItems = (
  items: IInsightsPost[] = [],
): ArticlesListingItem[][] => {
  const posts = getInsightsItems(items);
  let groupedItems: ArticlesListingItem[][] = [];
  let pageNumber = 1;

  do {
    const itemsPerPage = pageNumber === 1 ? 18 : 16;
    const pageItems = posts.splice(0, itemsPerPage);
    groupedItems = [...groupedItems, pageItems];

    // eslint-disable-next-line no-plusplus
    pageNumber++;
  } while (posts.length);

  return groupedItems;
};

export const getInsightsTags = (tags: IInsightTag[]): PageFilterItem[] =>
  tags.map((tag) => ({
    label: tag.name,
    to: `${routes.INSIGHT_TAG}${tag.slug}`,
    key: String(tag.id),
  }));

export const getInsightAuthor = (
  author: IInsightPostAuthor,
): ArticleLayoutAuthor => ({
  name: getFirstWord(author.name),
  email: author.email,
  image: {
    src: author.avatar,
    alt: `${author.name} - picture`,
  },
});

const regexToRemoveIE8Script = RegExp(
  /<!--\[if lte IE 8\]>(.|\n)*?<!\[endif\]-->/,
  'gmi',
);

const removePattern = (content: string, pattern = `<p></p>`) =>
  content.replace(pattern, '');

export const getPostContent = (
  content: string,
  shortContent: string,
  partToRemove = '<!--more-->',
): string =>
  content
    .replace(regexToRemoveIE8Script, '')
    .replace(partToRemove, '')
    .replace(removePattern(shortContent), '')
    .replace(/font-size:\s?\d+px;/g, '');

const removeDuplicates = (arr: string[]): string[] =>
  arr.filter((value, currIndex) => arr.indexOf(value) === currIndex);

const getMatches = (content: string, regex: RegExp): string[] => {
  let scripts;
  const matches = [];
  do {
    scripts = regex.exec(content);
    if (scripts) {
      matches.push(scripts[1]);
    }
  } while (scripts);

  return removeDuplicates(matches.filter(Boolean));
};
export const findContentScript = (content: string): string[] => {
  const regexScriptTag = RegExp(
    '<script?\\w+(?:\\s+(?:src="([^"]*)")|[^\\s>]+|\\s+)*>',
    'gi',
  );

  const matches = getMatches(content, regexScriptTag);

  return matches;
};
