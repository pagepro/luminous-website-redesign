export default {
  link: {
    color: 'inherit',
    transition: '250ms ease-in-out',
    svg: {
      fontSize: '1.875rem',
    },
  },
};
