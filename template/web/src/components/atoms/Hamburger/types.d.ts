export interface HamburgerProps {
  onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  isOpen?: boolean;
}
