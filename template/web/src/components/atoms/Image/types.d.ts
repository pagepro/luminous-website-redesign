import { FixedObject, FluidObject, GatsbyImageProps } from 'gatsby-image';

export interface ImageProps {
  fixed?: FixedObject;
  fluid?: FluidObject;
  ratio?: number;
  alt?: string;
  src?: string;
}

export type ImagePropsType = ImageProps &
  Omit<GatsbyImageProps, 'fluid' | 'fixed'>;
