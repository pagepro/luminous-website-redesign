export default {
  wrapper: {
    width: '100%',
    height: '100%',
    alignItems: 'flex-start',
    '.gatsby-image-wrapper': {
      width: '100%',
      height: '100%',
      transition: '400ms ease-in-out',
    },
  },
  media: {
    width: '100%',
    height: 'auto',
    display: 'block',
    objectFit: 'cover',
  },
};
