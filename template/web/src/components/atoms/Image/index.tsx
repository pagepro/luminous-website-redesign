import React from 'react';
import { AspectRatio, Flex, Image as ThemedImage } from 'theme-ui';
import GatsbyImage from 'gatsby-image';
import { ImagePropsType } from './types';

const Image: React.FC<ImagePropsType> = ({
  fluid,
  fixed,
  ratio,
  alt = '',
  src,
  ...gatbyImageProps
}) => {
  const gifSrc = fluid?.src || fixed?.src;
  const isGif = gifSrc?.includes('.gif');

  const imageContent = (
    <Flex variant="image.wrapper">
      {(src || isGif) && (
        <ThemedImage
          variant="image.media"
          src={isGif ? gifSrc : src}
          {...{ alt }}
        />
      )}
      {!src && !isGif && fluid && !fixed && (
        <GatsbyImage
          loading="eager"
          fadeIn={false}
          {...{ alt, fluid, ...gatbyImageProps }}
        />
      )}
      {!src && !isGif && fixed && !fluid && (
        <GatsbyImage
          loading="eager"
          fadeIn={false}
          {...{ alt, fixed, ...gatbyImageProps }}
        />
      )}
    </Flex>
  );

  return ratio ? (
    <AspectRatio {...{ ratio }}>{imageContent}</AspectRatio>
  ) : (
    imageContent
  );
};

export default Image;
