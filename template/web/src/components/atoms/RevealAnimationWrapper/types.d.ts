export interface RevealAnimationWrapperProps {
  cascade?: boolean;
  delay?: number;
  fraction?: number;
  damping?: number;
  css?: Interpolation;
  className?: string;
  style?: React.CSSProperties;
  childClassName?: string;
  childStyle?: React.CSSProperties;
}
