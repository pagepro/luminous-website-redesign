import React from 'react';

export const RevealAnimationContext = React.createContext(false);
