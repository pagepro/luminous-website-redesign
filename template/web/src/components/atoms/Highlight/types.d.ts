import { MotionProps } from 'framer-motion';

export type HighlightProps = MotionProps;
