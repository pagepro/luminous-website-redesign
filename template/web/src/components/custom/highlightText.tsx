import React from 'react';
import Highlight from '../atoms/Highlight';

const highlight = (props: { children: React.ReactNode }): React.ReactNode => (
  <Highlight custom={1}>{props.children}</Highlight>
);

export default highlight;
