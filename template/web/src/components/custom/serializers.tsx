import HighlightText from './highlightText';

const serializers = {
  marks: { mark: HighlightText },
};

export default serializers;
