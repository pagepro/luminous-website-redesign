export type ICategory = {
  id: string;
  title: string;
  slug: ISlug;
};
export type IWork = {
  clientName: string;
  title: string;
  mainImage: IImage;
  darkBackground: boolean;
  slug: ISlug;
};
export type IInfoBanner = {
  _type: 'infoBanner';
  title: string;
  quote: string;
  subtitle: string;
  link: (IAddressEmail | IMenuItem)[];
};
export type IInfoBannerPopup = {
  _type: 'infoBannerPopup';
  title: string;
  quote: string;
  buttonText: string;
  popupHeading: string;
};

export type IAddressEmail = {
  title: string;
  email: string;
  _type: 'addressEmail';
};
export type IMenuItem = {
  link: string;
  nofollow: boolean;
  openNewTab: boolean;
  title: string;
  _key: string;
  _type: 'menuItem';
};

export type IContentSection = {
  _key: string;
  _type:
    | 'effectiveNumbers'
    | 'simpleContent'
    | 'headingContent'
    | 'video'
    | 'videoUrl'
    | 'singleImage'
    | 'twoImages'
    | 'awards'
    | 'images';
  _rawBody?: IContent[];
  heading?: string;
  text?: string;
  mainImage?: IImage;
  firstImage?: IImage;
  secondImage?: IImage;
  placeholderImage?: IImage;
  video?: IVideo;
  awardsList?: string[];
  videoUrl?: string;
};

export interface ICasyStudyThumbnail {
  id: string;
  title: string;
  clientName: string;
  mainImage: IImage;
  mainVideo?: IVideo;
  darkBackground: boolean;
  slug: ISlug;
}

export type ICasyStudy = {
  title: string;
  clientName: string;
  seoTitle?: string;
  seoDescription?: string;
  seoImage?: ISeoImage;
  tag: ITag[];
  mainImage: IImage;
  mainVideo?: IVideo;
  sections: IContentSection[];
  works: IWork[];
  infoBanner: IInfoBanner[];
};

export type ITeamMember = {
  id: string;
  name: string;
  position: string;
  email: string;
  mainImage: IImage;
  _rawDescription: IContent[];
};

export type ICarouselItem = {
  _key: string;
  heading: string;
  _rawBody: IContent;
};
export type IThumbnailQuote = {
  _key: string;
  title: string;
  authorName: string;
  positionName: string;
  quote: string;
  image: IImage;
};

export type ICarouselQuote = {
  heading: string;
  quotes: IThumbnailQuote[];
};
