declare module '@theme-ui/match-media' {
  type Options = {
    defaultIndex?: number;
  };

  export function useResponsiveValue(
    params: boolean[],
    options?: Options,
  ): boolean;
}
