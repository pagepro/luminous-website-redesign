export const regexIndexOf = (
  string: string,
  regex: RegExp,
  startpos = 0,
): number => {
  const indexOf = string.substring(startpos).search(regex);

  return indexOf >= 0 ? indexOf + startpos : startpos;
};
