export const convertNewLinesToBreakLines = (text: string): string =>
  text.replace(/\n/gi, '<br/>');
