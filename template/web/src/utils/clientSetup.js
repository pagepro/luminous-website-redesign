const sanityClient = require('@sanity/client');

const client = sanityClient({
  projectId: process.env.GATSBY_SANITY_PROJECT_ID,
  dataset: process.env.GATSBY_SANITY_DATASET,
  token: process.env.SANITY_READ_TOKEN,
  useCdn: true,
});

module.exports = {
  client,
};
