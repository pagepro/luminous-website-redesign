const createPages = async (graphql, actions) => {
  const { createPage } = actions;
  const result = await graphql(`
    {
      allSanityPage {
        nodes {
          id
          slug {
            slug {
              current
            }
          }
        }
      }
      allSanityInsightShortcut(limit: 4, sort: {order: DESC, fields: date}) {
        nodes {
          imageUrl
          title
          description
          date(formatString: "DD.MM.YY")
          author
          id
          slug {
            current
          }
        }
      }
    }
  `);

  if (result.errors) throw result.errors;

  const pages = (result.data.allSanityPage || {}).nodes || [];
  const lastInsights = result.data.allSanityInsightShortcut.nodes

  pages.forEach((page) => {
    const { id, slug } = page;
    const path = `${slug.slug.current}`;

    createPage({
      path,
      component: require.resolve('../../templates/page.tsx'),
      context: { id, lastInsights },
    });
  });
};
module.exports = {
  createPages,
};
