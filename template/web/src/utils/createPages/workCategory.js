const createWorkCategoryPages = async (graphql, actions, pathPrefix) => {
  const { createPage } = actions;
  const result = await graphql(`
    {
      allSanityCategory {
        nodes {
          id
          slug {
            current
          }
        }
      }
    }
  `);

  if (result.errors) throw result.errors;
  const workCategories = (result.data.allSanityCategory || {}).nodes || [];
  workCategories.forEach((category) => {
    const { id, slug = {} } = category;
    const isMainPage = slug.current === '/';
    const path = isMainPage ? '/work' : `${pathPrefix}${slug.current}`;

    createPage({
      path,
      component: require.resolve('../../templates/works.tsx'),
      context: { id },
    });
  });
};
module.exports = {
  createWorkCategoryPages,
};
