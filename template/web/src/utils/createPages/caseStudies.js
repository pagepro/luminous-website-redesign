const createCaseStudiesPages = async (graphql, actions, pathPrefix) => {
  const { createPage } = actions;
  const result = await graphql(`
    {
      allSanityWork {
        nodes {
          id
          slug {
            current
          }
        }
      }
    }
  `);

  if (result.errors) throw result.errors;

  const caseStudies = (result.data.allSanityWork || {}).nodes || [];
  caseStudies.forEach((category) => {
    const { id, slug = {} } = category;
    const path = `${pathPrefix}${slug.current}`;

    createPage({
      path,
      component: require.resolve('../../templates/caseStudy.tsx'),
      context: { id },
    });
  });
};

module.exports = {
  createCaseStudiesPages,
};
