const createJobsPages = async (graphql, actions, pathPrefix) => {
  const { createPage } = actions;
  const result = await graphql(`
    {
      allSanityJob {
        nodes {
          slug {
            current
          }
          id
        }
      }
    }
  `);

  if (result.errors) throw result.errors;
  const jobs = (result.data.allSanityJob || {}).nodes || [];
  jobs.forEach((job) => {
    const { id, slug = {} } = job;
    const path = `${pathPrefix}${slug.current}`;

    createPage({
      path,
      component: require.resolve('../../templates/job.tsx'),
      context: { id },
    });
  });
};
module.exports = {
  createJobsPages,
};
