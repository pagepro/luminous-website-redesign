const {
  getPosts,
  getTags,
  getGroupedPosts,
  getRelatedPosts,
  updateInsightsShortcuts,
} = require('../getHubSpotData');

const createBlogPost = async (graphql, actions, pathPrefix) => {
  const { createPage } = actions;

  const result = await graphql(`
    {
      allSanityInsightShortcut {
        nodes {
          _id
        }
      }
    }
  `);

  if (result.errors) throw result.errors;

  const posts = await getPosts();
  const blogTags = await getTags();

  const tags = (blogTags.data.objects || [])
    .sort((a, b) => b.livePosts - a.livePosts)
    .map(({ id, name, slug }) => ({
      id,
      name,
      slug,
    }));

  const groupedPostsByTagID = getGroupedPosts(posts);

  tags.forEach(({ id, slug }) => {
    const path = `${pathPrefix}/tag/${slug}`;

    createPage({
      path,
      component: require.resolve('../../templates/insights.tsx'),
      context: { posts: groupedPostsByTagID[id], tags },
    });
  });

  if (process.env.NODE_ENV === 'production') {
    const postsFromCms =
      (result.data.allSanityInsightShortcut || {}).nodes || [];

    await updateInsightsShortcuts(posts, postsFromCms);
  }

  posts.forEach((post) => {
    const path = `${pathPrefix}/${post.slug}`;
    const firstTag = post.topic_ids[0];
    const postsByTagId = groupedPostsByTagID[firstTag] || [];
    const relatedPosts = getRelatedPosts(postsByTagId, post.id);

    createPage({
      path,
      component: require.resolve('../../templates/insight.tsx'),
      context: { post, relatedPosts },
    });
  });

  createPage({
    path: pathPrefix,
    component: require.resolve('../../templates/insights.tsx'),
    context: { posts, tags },
  });
};
module.exports = {
  createBlogPost,
};
