import sanityClient from '@sanity/client';

export const client = sanityClient({
  projectId: process.env.GATSBY_SANITY_PROJECT_ID as string,
  dataset: process.env.GATSBY_SANITY_DATASET as string,
  useCdn: false,
  withCredentials: true,
});
