/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
const axios = require('axios');
const Moment = require('moment');
const _ = require('lodash');
const { client } = require('./clientSetup');

const getHubSpotData = (endpoint, params = {}) =>
  axios.get(`${process.env.GATSBY_HUBSPOT_API_URL}${endpoint}`, {
    params: {
      hapikey: process.env.GATSBY_HUBSPOT_API_KEY,
      ...params,
    },
  });

const getGroupedPosts = (posts) =>
  posts.reduce((results, post) => {
    const topics = post.topic_ids;
    topics.forEach((topic) => {
      if (!results[topic]) {
        // eslint-disable-next-line no-param-reassign
        results[topic] = [post];
      } else {
        results[topic].push(post);
      }
    });
    return results;
  }, {});

const getPosts = async (
  results = [],
  offset = 0,
  limit = 300,
  archived = false,
  state = 'PUBLISHED',
) => {
  const { data } = await getHubSpotData(
    process.env.GATSBY_HUBSPOT_BLOG_POSTS_URL,
    {
      archived,
      state,
      limit,
      offset,
    },
  );
  const newOffset = offset + limit;
  const newResults = [...results, ...data.objects];

  if (data.total > newOffset) {
    const response = await getPosts(newResults, newOffset);
    return response;
  }
  return newResults;
};

const getTags = async () => {
  const tags = await getHubSpotData(process.env.GATSBY_HUBSPOT_TOPICS_URL, {
    active: true,
  });

  return tags;
};

const getRelatedPosts = (list, excludedId, limit = 4) =>
  list.filter((item) => item.id !== excludedId).slice(0, limit);

const stripTags = (str) =>
  str
    .replace(/<\/?[^>]+(>|$)/g, '')
    .replace(/&nbsp;/g, ' ')
    .trim();

const getDate = (timestamp, format = 'YYYY-MM-DD') =>
  Moment(timestamp).format(format);

const prefixInsightId = 'insight-';

const transformPostToShortcut = (post) => ({
  _id: `${prefixInsightId}${post.id}`,
  _type: 'insightShortcut',
  title: post.title,
  date: getDate(post.publish_date),
  description: stripTags(post.post_list_content),
  author: post.blog_post_author.name,
  imageUrl: post.post_list_summary_featured_image
    ? post.post_list_summary_featured_image
    : post.featured_image,
  slug: {
    _type: 'slug',
    current: post.slug,
  },
});

const createPosts = async (postsGroups) => {
  for (const posts of postsGroups) {
    await Promise.all(
      posts.map((post) =>
        client.createOrReplace(transformPostToShortcut(post)),
      ),
    );
  }
};
const removeOldPosts = async (idsGroups) => {
  for (const ids of idsGroups) {
    await Promise.all(ids.map((id) => client.delete(id)));
  }
};

const updateInsightsShortcuts = async (newPosts, oldPosts = []) => {
  const newPostsIds = newPosts.map(({ id }) => `${prefixInsightId}${id}`);
  const oldPostsIds = oldPosts.map(({ _id }) => _id);

  const postsToRemove = oldPostsIds.filter((id) => !newPostsIds.includes(id));
  // we have to chunk array to small pieces, there is problem with to many request at the same time
  await removeOldPosts(_.chunk(postsToRemove, 20));
  await createPosts(_.chunk(newPosts, 20));
};

module.exports = {
  getPosts,
  getTags,
  getGroupedPosts,
  getRelatedPosts,
  updateInsightsShortcuts,
};
