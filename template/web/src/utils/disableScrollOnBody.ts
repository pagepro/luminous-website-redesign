const disableScrollOnBody = (): void => {
  const scrollY = `${window.scrollY}px`;
  const { body } = document;

  body.classList.add('is-fixed');
  body.style.top = `-${scrollY}`;
};

export default disableScrollOnBody;
