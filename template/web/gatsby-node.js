const { createPages } = require('./src/utils/createPages/pages');
const {
  createCaseStudiesPages,
} = require('./src/utils/createPages/caseStudies');
const {
  createWorkCategoryPages,
} = require('./src/utils/createPages/workCategory');
const { createBlogPost } = require('./src/utils/createPages/blogPosts');
const { createJobsPages } = require('./src/utils/createPages/jobs');

exports.createPages = async ({ graphql, actions }) => {
  await createWorkCategoryPages(graphql, actions, '/work/discipline/');
  await createCaseStudiesPages(graphql, actions, '/case-study/');
  await createBlogPost(graphql, actions, '/insights');
  await createJobsPages(graphql, actions, '/join/opportunities/');
  await createPages(graphql, actions);
};

// const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

// exports.onCreateWebpackConfig = ({ actions }) => {
//   actions.setWebpackConfig({
//     resolve: {
//       plugins: [new TsconfigPathsPlugin()],
//     },
//   });
// };

// exports.onCreateNode = ({ node }) => {
//   console.log(`Node created of type "${node}"`, node);
// };
