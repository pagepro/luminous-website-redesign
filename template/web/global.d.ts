declare global {
  import React from 'react';
  import { FluidObject } from 'gatsby-image';

  type SvgComponentType = React.FunctionComponent<
    React.SVGProps<SVGSVGElement>
  >;

  interface ChildImageSharpFluid {
    childImageSharp: {
      fluid: FluidObject;
    };
  }

  type GQLImages<N> = Record<N, ChildImageSharpFluid>;

  type ISlug = {
    current: string;
  };
  type ITag = {
    name: string;
  };
  type ISeoImage = {
    asset: {
      fluid: {
        src: string;
      };
    };
  };
  type IImage = {
    _type: 'image';
    _key: string;
    asset: {
      fluid: {
        aspectRatio: number;
        base64: string;
        sizes: string;
        src: string;
        srcSet: string;
        srcSetWebp: string;
        srcWebp: string;
      };
    };
  };
  type IVideo = {
    asset?: {
      url?: string;
    };
  };
  type IContent = {
    _key: string;
    _type: string;
    children: [
      {
        _key: string;
        _type: string;
        marks: ['mark'];
        text: string;
      },
    ];
    markDefs: [];
    style: string;
  };
}

export {};
