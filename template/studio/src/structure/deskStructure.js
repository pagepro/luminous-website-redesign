import S from '@sanity/desk-tool/structure-builder'
import landingPages from '../../schemas/structure/landingPages'
import tags from '../../schemas/structure/tags'
import works from '../../schemas/structure/works'
import teams from '../../schemas/structure/teams'
import categories from '../../schemas/structure/categories'
import IframePreview from '../previews/IframePreview'
import jobOffers from '../../schemas/structure/jobOffers'
import insightsShortcuts from '../../schemas/structure/insightsShortcuts'

// Web preview configuration
// const remoteURL = '<#<deployments.web.url>#>'
const remoteURL = 'https://luminous-website.netlify.app'
const localURL = 'http://localhost:8000'
const previewURL = window.location.hostname === 'localhost' ? localURL : remoteURL

const hiddenDocTypes = listItem =>
  ![
    'work',
    'team',
    'tag',
    'category',
    'navigationMenu',
    'job',
    'insightShortcut',
    'page',
    'route',
    'menu'
  ].includes(listItem.getId())

export const getDefaultDocumentNode = props => {
  /**
   * Here you can define fallback views for document types without
   * a structure definition for the document node. If you want different
   * fallbacks for different types, or document values (e.g. if there is a slug present)
   * you can set up that logic in here too.
   * https://www.sanity.io/docs/structure-builder-reference#getdefaultdocumentnode-97e44ce262c9
   */
  const { schemaType } = props
  if (['work', 'category', 'page'].includes(schemaType)) {
    return S.document().views([
      S.view.form(),
      S.view
        .component(IframePreview)
        .title('Web preview')
        .options({ previewURL })
    ])
  }
  return S.document().views([S.view.form()])
}

/**
 * This defines how documents are grouped and listed out in the Studio.
 * Relevant documentation:
 * - https://www.sanity.io/guides/getting-started-with-structure-builder
 * - https://www.sanity.io/docs/structure-builder-introduction
 * - https://www.sanity.io/docs/structure-builder-typical-use-cases
 * - https://www.sanity.io/docs/structure-builder-reference
 */

export default () =>
  S.list()
    .title('Content')
    .items([
      works,
      teams,
      jobOffers,
      tags,
      categories,
      landingPages,
      insightsShortcuts,
      // `S.documentTypeListItems()` returns an array of all the document types
      // defined in schema.js. We filter out those that we have
      // defined the structure above.
      ...S.documentTypeListItems().filter(hiddenDocTypes)
    ])
