import React from 'react'

const HighlightRender = props => <span style={{ backgroundColor: 'yellow' }}>{props.children}</span>

export default HighlightRender
