import React from 'react'

const HighlightIcon = () => <span style={{ backgroundColor: 'yellow' }}>&nbsp;H&nbsp;</span>

export default HighlightIcon
