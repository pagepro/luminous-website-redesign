export default {
  name: 'effectiveNumbers',
  type: 'object',
  title: 'Highly effective numbers',
  fields: [
    {
      name: 'heading',
      type: 'string',
      title: 'Title'
    },
    {
      name: 'text',
      type: 'text',
      title: 'Description'
    },
  ],
  preview: {
    select: {
      title: 'heading',
      subtitle: 'text'
    }
  }
}
