export default {
  name: 'twoImages',
  type: 'object',
  title: 'Two pictures in a row',
  fields: [
    {
      name: 'firstImage',
      type: 'image',
      title: 'First image'
    },
    {
      name: 'secondImage',
      type: 'image',
      title: 'Second image'
    }
  ],
  preview: {
    select: {
      media: 'firstImage'
    },
    prepare({ media }) {
      return {
        title: 'Two pictures in a row',
        media
      }
    }
  }
}
