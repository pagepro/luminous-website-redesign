export default {
  name: 'contentText',
  type: 'object',
  title: 'Content & Text',
  fields: [
    {
      name: 'body',
      type: 'highlightTitle',
      title: 'Content'
    },
    { type: 'text', name: 'text' }
  ],
  preview: {
    select: {
      title: 'body',
      subtitle: 'text'
    }
  }
}
