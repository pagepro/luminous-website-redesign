import { validationUrl } from '../utils'

export default {
  name: 'mapItem',
  type: 'object',
  title: 'Map item',
  fields: [
    {
      type: 'string',
      name: 'title'
    },
    {
      type: 'text',
      name: 'address'
    },
    {
      type: 'url',
      name: 'mapUrl',
      validation: Rule => validationUrl(Rule)
    },
    {
      type: 'string',
      name: 'phoneNumber'
    },
    {
      type: 'url',
      name: 'zoom',
      title: 'Link to ZOOM meeting',
      description: 'Add phone number to see a button with meeting',
      validation: Rule => validationUrl(Rule)
    },
    {
      type: 'image',
      name: 'image',
      title: 'Map image'
    },
    {
      type: 'positionInputs',
      name: 'pinPositionX',
      description: 'Set a number between 0-100',
      title: 'Cursor positon from left'
    },
    {
      type: 'positionInputs',
      description: 'Set a number between 0-100',
      title: 'Cursor positon from top',
      name: 'pinPositionY'
    }
  ],
  preview: {
    select: {
      title: 'title'
    },
    prepare({ title = 'Map item' }) {
      return {
        title
      }
    }
  }
}
