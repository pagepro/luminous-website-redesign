export default {
  name: 'teamMembersSection',
  type: 'object',
  title: 'Team members Section',
  fields: [
    {
      type: 'string',
      name: 'title',
      title: 'Heading'
    },
    {
      name: 'teamMembers',
      type: 'array',
      title: 'Team Members',
      of: [
        {
          type: 'reference',
          to: {
            type: 'team'
          }
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title'
    }
  }
}
