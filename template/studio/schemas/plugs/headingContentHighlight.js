export default {
  name: 'headingContentHighlight',
  type: 'object',
  title: 'Heading & Content',
  fields: [
    {
      name: 'heading',
      type: 'string',
      title: 'Heading'
    },
    {
      name: 'body',
      type: 'highlightTitle',
      title: 'Content'
    }
  ],
  preview: {
    select: {
      title: 'heading',
      subtitle: 'body'
    }
  }
}
