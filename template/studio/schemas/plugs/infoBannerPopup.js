export default {
  name: 'infoBannerPopup',
  type: 'object',
  title: 'Info Banner with Popup',
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title'
    },
    {
      name: 'quote',
      type: 'text',
      title: 'Text'
    },
    {
      name: 'buttonText',
      type: 'string',
      title: 'Button text'
    },
    {
      name: 'popupHeading',
      type: 'string'
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'quote'
    }
  }
}
