export default {
  name: 'stickyList',
  type: 'object',
  title: 'Sticky list',
  fields: [
    {
      type: 'string',
      name: 'title',
      title: 'Heading'
    },
    {
      name: 'mediaItem',
      type: 'array',
      title: 'Media',
      validation: Rule => Rule.max(1),
      of: [
        {
          type: 'video'
        },
        {
          type: 'videoUrl'
        },
        {
          type: 'image'
        }
      ]
    },
    {
      title: 'Set Image in right column',
      description: 'It works only if you select Image in Media list',
      name: 'isReverseColumn',
      type: 'boolean',
      options: { layout: 'checkbox' }
    },
    {
      name: 'list',
      type: 'array',
      title: 'Items list',
      of: [
        {
          type: 'headingText'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title'
    }
  }
}
