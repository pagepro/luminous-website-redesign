export default {
  name: 'thumbnailQuotesBlock',
  type: 'object',
  title: 'Carousel Quotes Section',
  fields: [
    {
      name: 'heading',
      type: 'string'
    },
    {
      name: 'quotes',
      type: 'array',
      title: 'Thumbnails list',
      of: [
        {
          type: 'thumbnailQuote'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'heading'
    },
    prepare({ title = 'No heading' }) {
      return {
        title
      }
    }
  }
}
