export default {
  name: 'listsWithSublists',
  type: 'object',
  title: 'Lists with Sublists',
  fields: [
    {
      type: 'string',
      name: 'title',
      title: 'Heading'
    },
    {
      name: 'listItems',
      type: 'array',
      title: 'Items list',
      of: [
        {
          type: 'headingListItems'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title'
    }
  }
}
