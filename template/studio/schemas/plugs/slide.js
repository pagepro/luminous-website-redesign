export default {
  name: 'slide',
  type: 'object',
  title: 'Slide',
  fieldsets: [
    {
      name: 'button',
      title: 'Button'
    }
  ],
  fields: [
    {
      name: 'title',
      type: 'string'
    },
    {
      name: 'setWhiteColorText',
      type: 'boolean',
      options: {
        layout: 'checkbox'
      }
    },
    {
      name: 'description',
      type: 'text'
    },
    {
      name: 'buttonLink',
      title: 'Url',
      type: 'string',
      fieldset: 'button',
      validation: Rule =>
        Rule.custom(link => {
          return typeof link === 'undefined' || link.startsWith('/')
            ? true
            : 'Link must start with /(local) to be valid'
        })
    },
    {
      name: 'buttonText',
      title: 'Text',
      type: 'string',
      fieldset: 'button'
    },
    {
      name: 'tag',
      type: 'string',
      title: 'Tag name'
    },
    {
      name: 'image',
      type: 'image',
      title: 'Background picture'
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'description'
    }
  }
}
