import { toPlainText } from '../utils'

export default {
  name: 'thumbnailPage',
  type: 'object',
  title: 'Thumbnail Page',
  fields: [
    {
      type: 'highlightTitle',
      name: 'body',
      title: 'Title'
    },
    {
      name: 'url',
      type: 'reference',
      title: 'Page url',
      to: [
        {
          type: 'route'
        }
      ]
    },
    {
      name: 'image',
      type: 'image',
      title: 'Image'
    },
    {
      type: 'boolean',
      name: 'isWorkRelated',
      title: "Alternative hover - 'Eye'",
      options: {
        layout: 'checkbox'
      }
    },
    {
      name: 'buttonText',
      type: 'string',
      title: 'Button Text'
    }
  ],
  preview: {
    select: {
      title: 'body',
      subtitle: 'buttonText',
      media: 'image'
    },
    prepare({ title = 'No title', subtitle, media }) {
      return {
        title: toPlainText(title),
        subtitle,
        media
      }
    }
  }
}
