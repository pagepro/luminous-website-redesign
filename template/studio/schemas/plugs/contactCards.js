export default {
  type: 'object',
  name: 'contactCards',
  fields: [
    {
      type: 'array',
      name: 'contactCards',
      title: 'Items list',
      of: [
        {
          type: 'contactCard'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title'
    },
    prepare() {
      return {
        title: 'Contact Cards'
      }
    }
  }
}
