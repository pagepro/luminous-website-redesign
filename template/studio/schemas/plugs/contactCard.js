export default {
  name: 'contactCard',
  type: 'object',
  title: 'Contact Card',
  fields: [
    {
      type: 'string',
      name: 'title'
    },
    {
      type: 'array',
      name: 'personInfo',
      of: [
        {
          type: 'personInfo'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title'
    }
  }
}
