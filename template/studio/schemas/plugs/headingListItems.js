export default {
  name: 'headingListItems',
  type: 'object',
  title: 'Heading & List items',
  fields: [
    {
      name: 'heading',
      type: 'string',
      title: 'Heading'
    },
    {
      name: 'list',
      type: 'array',
      title: 'Items list',
      of: [
        {
          type: 'string'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'heading',
      subtitle: 'body'
    }
  }
}
