export default {
  name: 'singleCaseStudy',
  type: 'object',
  title: 'Row with Case Study',
  fields: [
    {
      name: 'caseStudy',
      type: 'array',
      title: 'Case Study',
      validation: Rule => Rule.max(1),
      of: [
        {
          type: 'reference',
          to: {
            type: 'work'
          }
        }
      ]
    }
  ],
  preview: {
    prepare() {
      return {
        title: 'Row with Case Study'
      }
    }
  }
}
