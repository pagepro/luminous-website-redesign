import { emailField } from '../consts/emailField'

export default {
  name: 'personInfo',
  type: 'object',
  title: 'Person Info',
  fields: [
    {
      type: 'string',
      name: 'title'
    },
    {
      type: 'string',
      name: 'name'
    },
    {
      type: 'string',
      name: 'profession'
    },
    emailField,
    {
      type: 'string',
      name: 'phoneNumber'
    }
  ],
  preview: {
    select: {
      title: 'title',
      name: 'name',
      profession: 'profession'
    },
    prepare({ title, name, profession }) {
      return {
        title: title ? title : name,
        subtitle: title ? name : profession
      }
    }
  }
}
