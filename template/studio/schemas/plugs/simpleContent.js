export default {
  name: 'simpleContent',
  type: 'object',
  title: 'Content',
  fields: [
    {
      name: 'body',
      type: 'bodyPortableText',
      title: 'Content'
    }
  ],
  preview: {
    select: {
      title: 'body'
    }
  }
}
