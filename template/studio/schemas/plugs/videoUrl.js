import { validationUrl } from '../utils'

export default {
  name: 'videoUrl',
  type: 'object',
  title: 'Full width VIMEO video',
  fields: [
    {
      title: 'Vimeo video url',
      name: 'videoUrl',
      type: 'url',
      validation: Rule => validationUrl(Rule)
    },
    {
      title: 'Set autoplay video',
      name: 'isAutoplay',
      type: 'boolean',
      options: { layout: 'checkbox' }
    },
    {
      name: 'placeholderImage',
      type: 'image',
      title: 'Placeholder image'
    }
  ],
  preview: {
    select: {
      media: 'placeholderImage'
    },
    prepare({ media }) {
      return {
        title: 'Full width VIMEO video',
        media
      }
    }
  }
}
