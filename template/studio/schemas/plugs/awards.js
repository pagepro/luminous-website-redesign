export default {
  name: 'awards',
  type: 'object',
  title: 'Awards',
  fields: [
    {
      name: 'heading',
      type: 'string',
      title: 'Title'
    },
    {
      name: 'awardsList',
      type: 'array',
      title: 'List',
      of: [
        {
          type: 'string'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'heading',
      award: 'awardsList.[0]'
    },
    prepare({ title = 'Awards', award = '' }) {
      return {
        title,
        subtitle: award
      }
    }
  }
}
