export default {
  name: 'headingContent',
  type: 'object',
  title: 'Heading & Content',
  fields: [
    {
      name: 'heading',
      type: 'string',
      title: 'Heading'
    },
    {
      name: 'body',
      type: 'bodyPortableText',
      title: 'Content'
    }
  ],
  preview: {
    select: {
      title: 'heading',
      subtitle: 'body'
    }
  }
}
