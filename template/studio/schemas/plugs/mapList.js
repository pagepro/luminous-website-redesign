export default {
  type: 'object',
  name: 'mapList',
  fields: [
    {
      type: 'string',
      name: 'title'
    },
    {
      type: 'array',
      name: 'mapList',
      title: 'Map items',
      of: [
        {
          type: 'mapItem'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title'
    },
    prepare({ title = 'Map item' }) {
      return {
        title
      }
    }
  }
}
