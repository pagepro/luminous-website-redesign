export default {
  name: 'twoCaseStudies',
  type: 'object',
  title: 'Row with two columns Case Studies',
  fields: [
    {
      name: 'caseStudy',
      type: 'array',
      title: 'First Case Study',
      validation: Rule => Rule.max(1),
      of: [
        {
          type: 'reference',
          to: {
            type: 'work'
          }
        }
      ]
    },
    {
      name: 'caseStudySecond',
      type: 'array',
      title: 'Second Case Study',
      validation: Rule => Rule.max(1),
      of: [
        {
          type: 'reference',
          to: {
            type: 'work'
          }
        }
      ]
    }
  ],
  preview: {
    prepare() {
      return {
        title: 'Row with two columns Case Studies'
      }
    }
  }
}
