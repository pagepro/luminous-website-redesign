export default {
  name: 'thumbnailQuote',
  type: 'object',
  title: 'Quote',
  fields: [
    {
      name: 'title',
      type: 'string'
    },
    {
      name: 'authorName',
      type: 'string',
      title: 'Author Name'
    },
    {
      name: 'positionName',
      type: 'string',
      title: 'Position Name'
    },
    {
      name: 'quote',
      type: 'text',
      title: 'Quote'
    },
    {
      name: 'image',
      type: 'image',
      title: 'Image'
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'quote',
      media: 'image'
    },
    prepare({ title = 'Quote', subtitle, media }) {
      return {
        title,
        subtitle,
        media
      }
    }
  }
}
