export default {
  name: 'video',
  type: 'object',
  title: 'Full width MP4 video',
  fields: [
    {
      title: 'Video file',
      name: 'video',
      type: 'file',
      options: {
        accept: 'video/*'
      }
    },
    {
      title: 'Set autoplay video',
      name: 'isAutoplay',
      type: 'boolean',
      options: { layout: 'checkbox' }
    },
    {
      name: 'placeholderImage',
      type: 'image',
      title: 'Placeholder image'
    }
  ],
  preview: {
    select: {
      media: 'placeholderImage'
    },
    prepare({ media }) {
      return {
        title: 'Full width MP4 video',
        media
      }
    }
  }
}
