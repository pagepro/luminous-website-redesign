export default {
  name: 'singleImage',
  type: 'object',
  title: 'Full width picture',
  fields: [
    {
      name: 'mainImage',
      type: 'image',
      title: 'Main image'
    }
  ],
  preview: {
    select: {
      media: 'mainImage'
    },
    prepare({ media }) {
      return {
        title: 'Full width picture',
        media
      }
    }
  }
}
