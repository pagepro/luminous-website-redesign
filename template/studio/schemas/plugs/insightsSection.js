import buttonLinkField from '../consts/buttonLinkField'

export default {
  name: 'insightsSection',
  type: 'object',
  title: 'Insights Section',
  fields: [
    {
      type: 'string',
      name: 'title',
      title: 'Heading'
    },
    {
      title: 'Ignore insight selection',
      description: 'Ignore selection and display the latest one from the API',
      name: 'shouldIgnoreSelection',
      type: 'boolean',
      options: { layout: 'checkbox' }
    },
    {
      name: 'insights',
      type: 'array',
      title: 'Insights',
      of: [
        {
          type: 'reference',
          to: {
            type: 'insightShortcut'
          }
        }
      ]
    },
    buttonLinkField
  ],
  preview: {
    select: {
      title: 'title'
    }
  }
}
