export default {
  name: 'jobsSection',
  type: 'object',
  title: 'Jobs Section',
  fields: [
    {
      name: 'jobs',
      type: 'array',
      title: 'Jobs list',
      validation: Rule => Rule.unique(),
      of: [
        {
          type: 'reference',
          to: {
            type: 'job'
          }
        }
      ]
    }
  ],
  preview: {
    prepare() {
      return {
        title: 'Jobs Section'
      }
    }
  }
}
