export default {
  name: 'infoBanner',
  type: 'object',
  title: 'Info Banner',
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title'
    },
    {
      name: 'quote',
      type: 'text',
      title: 'Quote'
    },
    {
      name: 'subtitle',
      type: 'string',
      title: 'Subtitle'
    },
    {
      name: 'link',
      type: 'array',
      title: 'Link',
      of: [{ type: 'menuItem' }, { type: 'addressEmail' }]
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'quote'
    }
  }
}
