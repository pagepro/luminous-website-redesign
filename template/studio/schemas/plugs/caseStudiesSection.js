import buttonLinkField from '../consts/buttonLinkField'

export default {
  name: 'caseStudiesSection',
  type: 'object',
  title: 'Case Studies Section',
  fields: [
    {
      type: 'string',
      name: 'title',
      title: 'Heading'
    },
    {
      name: 'caseStudies',
      type: 'array',
      title: 'Case Studies',
      of: [
        {
          type: 'reference',
          to: {
            type: 'work'
          }
        }
      ]
    },
    buttonLinkField
  ],
  preview: {
    select: {
      title: 'title'
    }
  }
}
