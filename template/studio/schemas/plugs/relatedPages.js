export default {
  name: 'relatedPages',
  type: 'object',
  title: 'Related Pages',
  fields: [
    {
      type: 'array',
      name: 'pagesList',
      title: 'Pages list',
      of: [
        {
          type: 'thumbnailPage'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'body'
    },
    prepare({ title = 'Related Pages' }) {
      return {
        title
      }
    }
  }
}
