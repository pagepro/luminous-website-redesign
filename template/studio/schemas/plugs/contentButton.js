import buttonLinkField from '../consts/buttonLinkField'

export default {
  name: 'contentButton',
  type: 'object',
  title: 'Content & Button',
  fields: [
    {
      name: 'body',
      type: 'highlightTitle',
      title: 'Content'
    },
    buttonLinkField
  ],
  preview: {
    select: {
      title: 'body'
    }
  }
}
