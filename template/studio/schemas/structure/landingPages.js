import S from '@sanity/desk-tool/structure-builder/'
import { MdMenu, MdWeb, MdLink } from 'react-icons/md'

export default S.listItem()
  .title('Page Builder')
  .child(
    S.list()
      .title('Landing pages')
      .items([
        S.listItem()
          .title('Main menu')
          .icon(MdMenu)
          .schemaType('menu')
          .child(S.document().schemaType('menu').documentId('menu')),
        S.listItem()
          .title('Navigation menu')
          .icon(MdMenu)
          .schemaType('navigationMenu')
          .child(S.documentTypeList('navigationMenu').title('Navigations Menus')),
        S.listItem()
          .title('Routes')
          .icon(MdLink)
          .schemaType('route')
          .child(S.documentTypeList('route').title('Routes')),
        S.listItem()
          .title('Pages')
          .icon(MdWeb)
          .schemaType('page')
          .child(S.documentTypeList('page').title('Pages'))
      ])
  )
