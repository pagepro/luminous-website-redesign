import S from '@sanity/desk-tool/structure-builder/'

export default S.listItem()
  .title('Job Offers')
  .schemaType('job')
  .child(S.documentTypeList('job').title('Job Offers'))
