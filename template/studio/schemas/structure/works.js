import S from '@sanity/desk-tool/structure-builder/'

export default S.listItem()
  .title('Works')
  .schemaType('work')
  .child(S.documentTypeList('work').title('Works'))
