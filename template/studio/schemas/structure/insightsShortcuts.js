import S from '@sanity/desk-tool/structure-builder/'

export default S.listItem()
  .title('Insights. Shortcuts')
  .schemaType('insightShortcut')
  .child(S.documentTypeList('insightShortcut').title('Insights. Shortcuts'))
