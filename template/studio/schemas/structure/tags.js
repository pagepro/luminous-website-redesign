import S from '@sanity/desk-tool/structure-builder/'

export default S.listItem()
  .title('Tags')
  .schemaType('tag')
  .child(S.documentTypeList('tag').title('Tags'))
