import S from '@sanity/desk-tool/structure-builder/'

export default S.listItem()
  .title('Team Members')
  .schemaType('team')
  .child(S.documentTypeList('team').title('Team Members'))
