// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator'

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type'

// document schemas
import work from './documents/work'
import team from './documents/team'
import tag from './documents/tag'
import category from './documents/category'
import navMenu from './documents/navMenu'
import job from './documents/job'
import insightShortcut from './documents/insightShortcut'
import page from './documents/page'
import route from './documents/route'
import menu from './documents/menu'

// Object types
import bodyPortableText from './objects/bodyPortableText'
import highlightTitle from './objects/highlightTitle'
import bioPortableText from './objects/bioPortableText'
import mainImage from './objects/mainImage'
import menuItem from './objects/menuItem'
import addressEmail from './objects/addressEmail'
import headingText from './objects/headingText'
import mainMenuItem from './objects/mainMenuItem'
import positionInputs from './objects/positionInputs'

// plugs
import simpleContent from './plugs/simpleContent'
import headingContent from './plugs/headingContent'
import singleImage from './plugs/singleImage'
import twoImages from './plugs/twoImages'
import video from './plugs/video'
import videoUrl from './plugs/videoUrl'
import effectiveNumbers from './plugs/effectiveNumbers'
import awards from './plugs/awards'
import infoBanner from './plugs/infoBanner'
import infoBannerPopup from './plugs/infoBannerPopup'
import singleCaseStudy from './plugs/singleCaseStudy'
import twoCaseStudies from './plugs/twoCaseStudies'
import headingContentHighlight from './plugs/headingContentHighlight'
import contentButton from './plugs/contentButton'
import caseStudiesSection from './plugs/caseStudiesSection'
import insightsSection from './plugs/insightsSection'
import stickyList from './plugs/stickyList'
import teamMembersSection from './plugs/teamMembersSection'
import contentText from './plugs/contentText'
import listsWithSublists from './plugs/listsWithSublists'
import headingListItems from './plugs/headingListItems'
import thumbnailPage from './plugs/thumbnailPage'
import relatedPages from './plugs/relatedPages'
import thumbnailQuote from './plugs/thumbnailQuote'
import thumbnailQuotesBlock from './plugs/thumbnailQuotesBlock'
import jobsSection from './plugs/jobsSection'
import contactCard from './plugs/contactCard'
import contactCards from './plugs/contactCards'
import personInfo from './plugs/personInfo'
import mapList from './plugs/mapList'
import mapItem from './plugs/mapItem'
import slide from './plugs/slide'

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: 'blog',
  // Then proceed to concatenate our our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    // The following are document types which will appear
    // in the studio.
    simpleContent,
    headingContent,
    singleImage,
    twoImages,
    video,
    videoUrl,
    effectiveNumbers,
    awards,
    infoBanner,
    infoBannerPopup,
    singleCaseStudy,
    twoCaseStudies,
    headingContentHighlight,
    contentButton,
    caseStudiesSection,
    insightsSection,
    stickyList,
    teamMembersSection,
    contentText,
    headingListItems,
    listsWithSublists,
    thumbnailPage,
    relatedPages,
    thumbnailQuote,
    thumbnailQuotesBlock,
    jobsSection,
    contactCard,
    contactCards,
    personInfo,
    mapList,
    mapItem,
    slide,
    work,
    team,
    category,
    tag,
    navMenu,
    job,
    insightShortcut,
    page,
    route,
    menu,
    mainImage,
    bodyPortableText,
    highlightTitle,
    bioPortableText,
    menuItem,
    addressEmail,
    headingText,
    mainMenuItem,
    positionInputs
  ])
})
