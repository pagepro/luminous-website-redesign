export default {
  name: 'tag',
  type: 'document',
  title: 'Tag',
  fields: [
    {
      name: 'name',
      type: 'string',
      title: 'Name'
    }
  ],
  orderings: [
    {
      name: 'tagNameASC',
      title: 'Tag Name [A - Z]',
      by: [
        {
          field: 'name',
          direction: 'asc'
        }
      ]
    },
    {
      name: 'tagNameDESC',
      title: 'Tag Name [Z - A]',
      by: [
        {
          field: 'name',
          direction: 'desc'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'name'
    }
  }
}
