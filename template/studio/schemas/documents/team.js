import { emailField } from '../consts/emailField'

export default {
  name: 'team',
  type: 'document',
  title: 'Team',
  fields: [
    {
      name: 'name',
      type: 'string',
      title: 'Name'
    },
    {
      name: 'position',
      type: 'string',
      title: 'Position'
    },
    emailField,
    {
      name: 'mainImage',
      type: 'image',
      title: 'Picture'
    },
    {
      name: 'description',
      type: 'bodyPortableText',
      title: 'Description'
    }
  ],
  orderings: [
    {
      name: 'teamNameASC',
      title: 'Team name [A - Z]',
      by: [
        {
          field: 'name',
          direction: 'asc'
        }
      ]
    },
    {
      name: 'teamNameDESC',
      title: 'Team name [Z - A]',
      by: [
        {
          field: 'name',
          direction: 'desc'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'name',
      subtitle: 'position',
      media: 'mainImage'
    },
    prepare({ title = 'No name', subtitle, media }) {
      return {
        title,
        subtitle,
        media
      }
    }
  }
}
