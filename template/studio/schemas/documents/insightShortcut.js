import React from 'react'
import { validationUrl } from '../utils'

export default {
  name: 'insightShortcut',
  type: 'document',
  title: 'Insights. Shortcuts',
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Post name'
    },
    {
      name: 'date',
      type: 'date',
      title: 'Date',
      options: {
        dateFormat: 'DD.MM.YY'
      }
    },
    {
      name: 'description',
      type: 'text',
      title: 'Short Description'
    },
    {
      name: 'author',
      type: 'string',
      title: 'Author'
    },
    {
      title: 'Thumbnail URL',
      name: 'imageUrl',
      type: 'url',
      validation: Rule => validationUrl(Rule)
    },
    {
      name: 'slug',
      type: 'slug',
      title: 'Slug',
      options: {
        source: 'title',
        maxLength: 96
      }
    }
  ],
  orderings: [
    {
      name: 'insightTitleASC',
      title: 'Insight Title [A - Z]',
      by: [
        {
          field: 'title',
          direction: 'asc'
        }
      ]
    },
    {
      name: 'insightTitleDESC',
      title: 'Insight Title [Z - A]',
      by: [
        {
          field: 'title',
          direction: 'desc'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'date',
      author: 'author',
      media: 'imageUrl'
    },
    prepare({ title = 'No title', subtitle, author, media }) {
      return {
        title,
        subtitle: `${author} / ${subtitle}`,
        media: <img src={media} />
      }
    }
  }
}
