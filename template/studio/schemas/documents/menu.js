export default {
  name: 'menu',
  type: 'document',
  title: 'Main menu',
  fields: [
    {
      name: 'elements',
      type: 'array',
      title: 'Elements menu',
      validation: Rule => Rule.min(1),
      of: [
        {
          type: 'mainMenuItem'
        }
      ]
    }
  ],
  preview: {
    prepare({ title = 'Main Menu' }) {
      return {
        title
      }
    }
  }
}
