import { seoFields, seoFieldset } from '../consts/seo'

export default {
  name: 'work',
  type: 'document',
  title: 'Work',
  fieldsets: [seoFieldset],
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title'
    },
    {
      name: 'clientName',
      type: 'string',
      title: 'Client Name'
    },
    {
      name: 'slug',
      type: 'slug',
      title: 'Slug',
      validation: Rule => Rule.required(),
      options: {
        source: 'title',
        maxLength: 96
      }
    },
    {
      name: 'mainImage',
      type: 'image',
      title: 'Main image'
    },
    {
      name: 'mainVideo',
      title: 'Main MP4 Video',
      type: 'file',
      options: {
        accept: 'video/*'
      }
    },
    {
      title: 'Thumbnail text color black',
      name: 'darkBackground',
      type: 'boolean',
      options: { layout: 'checkbox' }
    },
    {
      name: 'sections',
      type: 'array',
      title: 'Page sections',
      description: 'Add, edit and reorder sections',
      of: [
        { type: 'simpleContent' },
        { type: 'headingContent' },
        { type: 'singleImage' },
        { type: 'twoImages' },
        { type: 'video' },
        { type: 'videoUrl' },
        { type: 'effectiveNumbers' },
        { type: 'awards' }
      ]
    },
    {
      name: 'infoBanner',
      type: 'array',
      title: 'Info Banner',
      of: [
        {
          type: 'infoBanner'
        }
      ]
    },
    {
      name: 'tag',
      title: 'Tags',
      type: 'array',
      validation: Rule => Rule.unique(),
      of: [
        {
          type: 'reference',
          to: {
            type: 'tag'
          }
        }
      ]
    },
    {
      name: 'categories',
      type: 'array',
      title: 'Categories',
      of: [
        {
          type: 'reference',
          to: {
            type: 'category'
          }
        }
      ]
    },
    {
      name: 'works',
      type: 'array',
      title: 'Related Works',
      validation: Rule => Rule.max(2),
      of: [
        {
          type: 'reference',
          to: {
            type: 'work'
          }
        }
      ]
    },
    ...seoFields
  ],
  orderings: [
    {
      name: 'workTitleASC',
      title: 'Work Title [A - Z]',
      by: [
        {
          field: 'title',
          direction: 'asc'
        }
      ]
    },
    {
      name: 'workTitleDESC',
      title: 'Work Title [Z - A]',
      by: [
        {
          field: 'title',
          direction: 'desc'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'clientName',
      slug: 'slug',
      media: 'mainImage'
    },
    prepare({ title = 'No title', subtitle, media }) {
      return {
        title,
        subtitle,
        media
      }
    }
  }
}
