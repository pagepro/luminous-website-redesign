import { emailField } from '../consts/emailField'

export default {
  name: 'job',
  type: 'document',
  title: 'Job Offer',
  fields: [
    {
      name: 'date',
      type: 'date',
      title: 'Offer Date',
      options: {
        dateFormat: 'DD.MM.YY'
      }
    },
    {
      name: 'name',
      type: 'string',
      title: 'Position Name'
    },
    {
      name: 'slug',
      type: 'slug',
      title: 'Slug',
      validation: Rule => Rule.required(),
      options: {
        source: 'name',
        maxLength: 96
      }
    },
    {
      name: 'overview',
      type: 'bodyPortableText',
      title: 'Overview text'
    },
    {
      name: 'summary',
      type: 'bodyPortableText',
      title: 'Role summary'
    },
    {
      name: 'description',
      description: 'Only visible on the list of offers',
      type: 'text',
      title: 'Short Description '
    },
    {
      ...emailField,
      title: 'Apply Email'
    }
  ],
  orderings: [
    {
      name: 'jobOfferTitleASC',
      title: 'Job Offer Title [A - Z]',
      by: [
        {
          field: 'name',
          direction: 'asc'
        }
      ]
    },
    {
      name: 'jobOfferTitleDESC',
      title: 'Job Offer Title [Z - A]',
      by: [
        {
          field: 'name',
          direction: 'desc'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'name',
      subtitle: 'date'
    },
    prepare({ title = 'Position Name', subtitle }) {
      return {
        title,
        subtitle
      }
    }
  }
}
