import { seoFields, seoFieldset } from '../consts/seo'

export default {
  name: 'page',
  type: 'document',
  title: 'Page',
  fieldsets: [seoFieldset],
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title'
    },
    {
      name: 'slug',
      type: 'reference',
      title: 'Route',
      validation: Rule => Rule.required(),
      to: [
        {
          type: 'route'
        }
      ]
    },
    {
      title: 'Page has submenu',
      name: 'hasSubmenu',
      type: 'boolean',
      options: { layout: 'checkbox' }
    },
    {
      title: 'Slider',
      description: 'Visibilty of the Section slider',
      name: 'showSectionSlider',
      type: 'boolean'
    },
    {
      name: 'slides',
      type: 'array',
      title: 'Section slides',
      of: [
        {
          type: 'slide'
        }
      ]
    },
    {
      name: 'sections',
      type: 'array',
      title: 'Page sections',
      description: 'Add, edit and reorder sections',
      of: [
        { type: 'headingContentHighlight' },
        { type: 'singleImage' },
        { type: 'video' },
        { type: 'videoUrl' },
        { type: 'contentText' },
        { type: 'contentButton' },
        {
          type: 'caseStudiesSection'
        },
        {
          type: 'insightsSection'
        },
        {
          type: 'stickyList'
        },
        {
          type: 'listsWithSublists'
        },
        {
          type: 'teamMembersSection'
        },
        {
          type: 'jobsSection'
        },
        {
          type: 'relatedPages'
        },
        {
          type: 'contactCards'
        },
        {
          type: 'mapList'
        }
      ]
    },
    {
      name: 'carouselQuotes',
      type: 'array',
      title: 'Carousel Quotes Section',
      validation: Rule => Rule.max(1),
      of: [
        {
          type: 'thumbnailQuotesBlock'
        }
      ]
    },
    {
      type: 'boolean',
      name: 'showTwitterSection',
      title: 'Display Twitter section',
      options: {
        layout: 'checkbox'
      }
    },
    {
      name: 'infoBanner',
      type: 'array',
      title: 'Info Banner',
      validation: Rule => Rule.max(1),
      of: [
        {
          type: 'infoBanner'
        },
        {
          type: 'infoBannerPopup'
        }
      ]
    },
    {
      name: 'carouselItems',
      type: 'array',
      title: 'Carousel items',
      of: [
        {
          type: 'headingContent'
        }
      ]
    },
    ...seoFields
  ],
  orderings: [
    {
      name: 'pageTitleASC',
      title: 'Page Name [A - Z]',
      by: [
        {
          field: 'title',
          direction: 'asc'
        }
      ]
    },
    {
      name: 'pageTitleDESC',
      title: 'Page Name [Z - A]',
      by: [
        {
          field: 'title',
          direction: 'desc'
        }
      ]
    }
  ],
  initialValue: {
    showSectionSlider: false,
    hasSubmenu: false,
    showTwitterSection: false
  },
  preview: {
    select: {
      title: 'title'
    },
    prepare({ title = 'No title' }) {
      return {
        title
      }
    }
  }
}
