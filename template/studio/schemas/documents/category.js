import { seoFields, seoFieldset } from '../consts/seo'

export default {
  name: 'category',
  type: 'document',
  title: 'Category',
  fieldsets: [seoFieldset],
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Name'
    },
    {
      name: 'slug',
      type: 'slug',
      title: 'Slug',
      validation: Rule => Rule.required(),
      options: {
        source: 'title',
        maxLength: 96
      }
    },
    {
      name: 'caseStudies',
      type: 'array',
      title: 'Case Studies',
      of: [{ type: 'singleCaseStudy' }, { type: 'twoCaseStudies' }]
    },
    ...seoFields
  ],
  orderings: [
    {
      name: 'categoryNameASC',
      title: 'Category Name [A - Z]',
      by: [
        {
          field: 'title',
          direction: 'asc'
        }
      ]
    },
    {
      name: 'categoryNameDESC',
      title: 'Category Name [Z - A]',
      by: [
        {
          field: 'title',
          direction: 'desc'
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title'
    }
  }
}
