export default {
  name: 'positionInputs',
  type: 'object',
  title: 'Positions',
  fields: [
    {
      type: 'number',
      name: 'mobile',
      validation: Rule => Rule.min(0).max(100)
    },
    {
      type: 'number',
      name: 'tablet',
      validation: Rule => Rule.min(0).max(100)
    },
    {
      type: 'number',
      name: 'desktop',
      validation: Rule => Rule.min(0).max(100)
    }
  ]
}
