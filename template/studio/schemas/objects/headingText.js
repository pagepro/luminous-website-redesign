export default {
  title: 'Heading & text',
  name: 'headingText',
  type: 'object',

  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'string'
    },
    {
      title: 'Text',
      name: 'text',
      type: 'text'
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'text'
    }
  }
}
