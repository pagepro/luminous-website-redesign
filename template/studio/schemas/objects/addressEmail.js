import { emailField } from '../consts/emailField'

export default {
  title: 'Address email',
  name: 'addressEmail',
  type: 'object',
  fieldsets: [
    {
      title: 'Additional attributes',
      name: 'linkAttributes'
    }
  ],
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'string'
    },
    emailField
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'email'
    },
  }
}
