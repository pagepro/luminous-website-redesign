import React from 'react'

import { highlightDecorator } from '../consts/highlightDecorator'

const ParagraphStyle = props => (
  <span style={{ fontFamily: 'Times new roman, Times, serif' }}>
    {props.children}
  </span>
)

export default {
  name: 'highlightTitle',
  type: 'array',
  title: 'Title body',
  of: [
    {
      type: 'block',
      title: 'Block',
      styles: [
        { title: 'H2', value: 'h2' },
        { title: 'H3', value: 'h3' },
        { title: 'H4', value: 'h4' },
        { title: 'H5', value: 'h5' },
        { title: 'H6', value: 'h6' },
        { title: 'Paragraph', value: 'normal' },
        {
          title: 'Paragraph alt',
          value: 'p',
          blockEditor: {
            render: ParagraphStyle
          }
        }
      ],
      lists: [],
      marks: {
        decorators: [highlightDecorator],
        annotations: []
      }
    }
  ]
}
