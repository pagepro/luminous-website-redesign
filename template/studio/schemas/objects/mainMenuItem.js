export default {
  name: 'mainMenuItem',
  type: 'object',
  title: 'Menu element',
  fields: [
    {
      type: 'string',
      name: 'title'
    },
    {
      name: 'linkType',
      type: 'string',
      options: {
        list: [
          { title: 'Internal', value: 'internal' },
          { title: 'External', value: 'external' }
        ],
        layout: 'radio'
      }
    },
    {
      type: 'reference',
      name: 'slug',
      title: 'Route',
      hidden: ({ parent }) => parent.linkType !== 'internal' && parent.linkType !== undefined,
      to: [
        {
          type: 'route'
        }
      ]
    },
    {
      name: 'url',
      type: 'url',
      validation: Rule =>
        Rule.uri({
          scheme: ['https', 'http']
        }),
      hidden: ({ parent }) => parent.linkType !== 'external'
    },
    {
      name: 'submenu',
      type: 'array',
      title: 'Elements submenu',
      validation: Rule => Rule.unique(),
      of: [
        {
          type: 'reference',
          to: {
            type: 'route'
          }
        }
      ]
    }
  ],
  preview: {
    select: {
      title: 'title'
    },
    prepare({ title = 'Menu element' }) {
      return {
        title
      }
    }
  }
}
