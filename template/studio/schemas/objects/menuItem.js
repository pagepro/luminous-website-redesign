export default {
  title: 'Link',
  name: 'menuItem',
  type: 'object',
  fieldsets: [
    {
      title: 'Additional attributes',
      name: 'linkAttributes'
    }
  ],
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'string'
    },
    {
      title: 'Link',
      description: 'Example: /about-us or https://www.google.com',
      name: 'link',
      type: 'string',
      validation: Rule =>
        Rule.custom(link => {
          return typeof link === 'undefined' || link.startsWith('/') || link.startsWith('http')
            ? true
            : 'Link must start with /(local) or http|https (external) to be valid'
        })
    },
    {
      title: 'NoFollow',
      name: 'nofollow',
      type: 'boolean',
      fieldset: 'linkAttributes',
      options: {
        layout: 'checkbox'
      }
    },
    {
      title: 'Open in new tab',
      name: 'openNewTab',
      type: 'boolean',
      fieldset: 'linkAttributes',
      options: {
        layout: 'checkbox'
      }
    }
  ],
  preview: {
    select: {
      title: 'title',
      link: 'link'
    },
    prepare({ title = 'No title', link }) {
      let subtitle = 'Link is empty'
      if (link?.startsWith('/')) {
        subtitle = `Route: ${link}`
      }
      if (link?.startsWith('http')) {
        subtitle = `External: ${link}`
      }
      return {
        title,
        subtitle
      }
    }
  }
}
