export const validationUrl = Rule =>
  Rule.uri({
    scheme: ['http', 'https']
  })

export const toPlainText = (blocks = []) => {
  return blocks
    .map(block => {
      if (block._type !== 'block' || !block.children) {
        return ''
      }
      return block.children.map(child => child.text).join('')
    })
    .join('')
}
