export const emailField = {
  name: 'email',
  type: 'string',
  title: 'Email',
  validation: Rule =>
    Rule.custom(email => {
      const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

      return typeof email === 'undefined' || emailRegex.test(email)
        ? true
        : 'Please enter a valid email address'
    })
}
