export const seoFieldset = {
  name: 'seo',
  title: 'SEO'
}

export const seoFields = [
  {
    name: 'seoTitle',
    fieldset: 'seo',
    title: 'Title',
    type: 'string'
  },
  {
    name: 'seoDescription',
    fieldset: 'seo',
    title: 'Description',
    type: 'text'
  },
  {
    name: 'seoImage',
    type: 'image',
    fieldset: 'seo',
    title: 'Image'
  }
]
