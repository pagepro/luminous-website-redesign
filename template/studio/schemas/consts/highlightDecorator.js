import HighlightIcon from '../../components/highlightIcon'
import HighlightRender from '../../components/highlightRender'

export const highlightDecorator = {
  title: 'Highlight',
  value: 'mark',
  blockEditor: {
    icon: HighlightIcon,
    render: HighlightRender
  }
}
