export default {
  name: 'button',
  type: 'array',
  title: 'Button',
  validation: Rule => Rule.max(1),
  of: [
    {
      type: 'menuItem'
    }
  ]
}
